/**************************************************************************************************
	Filename:       notifyProfile.h
	Revised:        $Date:2016-07-21 16:45:53 $

	Description:    This file contains the GATT profile definitions and prototypes..

	Copyright 2013 EPLAB National Tsing Hua University. All rights reserved.
	The information contained herein is confidential property of NTHU. 	The material may be used for a personal and non-commercial use only in connection with 	a legitimate academic research purpose. Any attempt to copy, modify, and distribute any portion of this source code or derivative thereof for commercial, political, or propaganda purposes is strictly prohibited. All other uses require explicit written permission from the authors and copyright holders. This copyright statement must be retained in its entirety and displayed in the copyright statement of derived source code or systems.
**************************************************************************************************/
/*********************************************************************
 * INCLUDES
 */
#include "bcomdef.h"
#include "OSAL.h"
#include "linkdb.h"
#include "att.h"
#include "gatt.h"
#include "gatt_uuid.h"
#include "gattservapp.h"
#include "gapbondmgr.h"

#include "notifyProfile.h"

/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * CONSTANTS
 */

#define SERVAPP_NUM_ATTR_SUPPORTED        8

/*********************************************************************
 * TYPEDEFS
 */

/*********************************************************************
 * GLOBAL VARIABLES
 */

// GATT Profile Service UUID: 0xCCC0
CONST uint8 notifyProfileServUUID[ATT_BT_UUID_SIZE] =
{
  LO_UINT16(NOTIFYPROFILE_SERV_UUID), HI_UINT16(NOTIFYPROFILE_SERV_UUID)
};

// ELP1 UUID: 0xCCC1
CONST uint8 ELP1UUID[ATT_BT_UUID_SIZE] =
{
  LO_UINT16(ELP1_UUID), HI_UINT16(ELP1_UUID)
};

// EPL2 UUID: 0xCCC2
CONST uint8 EPL2UUID[ATT_BT_UUID_SIZE] =
{
  LO_UINT16(EPL2_UUID), HI_UINT16(EPL2_UUID)
};

/*********************************************************************
 * EXTERNAL VARIABLES
 */

/*********************************************************************
 * EXTERNAL FUNCTIONS
 */

/*********************************************************************
 * LOCAL VARIABLES
 */

static notifyProfileCBs_t *notifyProfile_AppCBs = NULL;

/*********************************************************************
 * Profile Attributes - variables
 */

// Profile Service attribute
static CONST gattAttrType_t notifyProfileService = { ATT_BT_UUID_SIZE, notifyProfileServUUID };

// Profile ELP1 Properties
static uint8 ELP1Props = GATT_PROP_READ | GATT_PROP_WRITE | GATT_PROP_NOTIFY;

// ELP1 Value
static uint8 ELP1Value = 0;// Profile ELP1 User Description
static uint8 ELP1UserDesp[17] = "characteristic 1\0";

// notifyProfile ELP1 Configuration Each client has its own
// instantiation of the Client Characteristic Configuration. Reads of the
// Client Characteristic Configuration only shows the configuration for
// that client and writes only affect the configuration of that client.
static gattCharCfg_t ELP1Config[GATT_MAX_NUM_CONN];

// Profile EPL2 Properties
static uint8 EPL2Props = GATT_PROP_READ | GATT_PROP_WRITE;

// EPL2 Value
static uint8 EPL2Value = 0;// Profile EPL2 User Description
static uint8 EPL2UserDesp[17] = "characteristic 2\0";

/*********************************************************************
 * Profile Attributes - Table
 */

static gattAttribute_t notifyProfileAttrTbl[SERVAPP_NUM_ATTR_SUPPORTED] = 
{
  // Simple Profile Service
  {
    { ATT_BT_UUID_SIZE, primaryServiceUUID }, /* type */
    GATT_PERMIT_READ,                         /* permissions */
    0,                                        /* handle */
    (uint8 *)&notifyProfileService           /* pValue */
  },

    // ELP1 Declaration
    {
      { ATT_BT_UUID_SIZE, characterUUID },
      GATT_PERMIT_READ,
      0,
      &ELP1Props
    },

      // ELP1 Value
      {
        { ATT_BT_UUID_SIZE, ELP1UUID },
        GATT_PERMIT_READ | GATT_PERMIT_WRITE,
        0,
        &ELP1Value
      },

      // ELP1 configuration
      {
        { ATT_BT_UUID_SIZE, clientCharCfgUUID },
        GATT_PERMIT_READ | GATT_PERMIT_WRITE,
        0,
        (uint8 *) ELP1Config
      },

      // ELP1 User Description
      {
        { ATT_BT_UUID_SIZE, charUserDescUUID },
        GATT_PERMIT_READ, 
        0,
        ELP1UserDesp
      },

    // EPL2 Declaration
    {
      { ATT_BT_UUID_SIZE, characterUUID },
      GATT_PERMIT_READ,
      0,
      &EPL2Props
    },

      // EPL2 Value
      {
        { ATT_BT_UUID_SIZE, EPL2UUID },
        GATT_PERMIT_READ | GATT_PERMIT_WRITE,
        0,
        &EPL2Value
      },

      // EPL2 User Description
      {
        { ATT_BT_UUID_SIZE, charUserDescUUID },
        GATT_PERMIT_READ, 
        0,
        EPL2UserDesp
      },


};


/*********************************************************************
 * LOCAL FUNCTIONS
 */
static uint8 notifyProfile_ReadAttrCB( uint16 connHandle, gattAttribute_t *pAttr, 
                            uint8 *pValue, uint8 *pLen, uint16 offset, uint8 maxLen );
static bStatus_t notifyProfile_WriteAttrCB( uint16 connHandle, gattAttribute_t *pAttr,
                                 uint8 *pValue, uint8 len, uint16 offset );

static void notifyProfile_HandleConnStatusCB( uint16 connHandle, uint8 changeType );


/*********************************************************************
 * PROFILE CALLBACKS
 */
// notifyProfileService Callbacks
CONST gattServiceCBs_t notifyProfileCBs =
{
  notifyProfile_ReadAttrCB,  // Read callback function pointer
  notifyProfile_WriteAttrCB, // Write callback function pointer
  NULL                       // Authorization callback function pointer
};

/*********************************************************************
 * PUBLIC FUNCTIONS
 */

/*********************************************************************
 * @fn      notifyProfile_AddService
 *
 * @brief   Initializes the notifyProfile service by registering
 *          GATT attributes with the GATT server.
 *
 * @param   services - services to add. This is a bit map and can
 *                     contain more than one service.
 *
 * @return  Success or Failure
 */
bStatus_t notifyProfile_AddService( uint32 services )
{
  uint8 status = SUCCESS;

  // Initialize Client Characteristic Configuration attributes
  GATTServApp_InitCharCfg( INVALID_CONNHANDLE, ELP1Config);

  // Register with Link DB to receive link status change callback
  VOID linkDB_Register( notifyProfile_HandleConnStatusCB );

  if ( services & NOTIFYPROFILE_SERVICE )
  {
    // Register GATT attribute list and CBs with GATT Server App
    status = GATTServApp_RegisterService( notifyProfileAttrTbl, 
                                          GATT_NUM_ATTRS( notifyProfileAttrTbl ),
                                          &notifyProfileCBs );
  }

  return ( status );
}


/*********************************************************************
 * @fn      notifyProfile_RegisterAppCBs
 *
 * @brief   Registers the application callback function. Only call
 *          this function once.
 *
 * @param   callbacks - pointer to application callbacks.
 *
 * @return  SUCCESS or bleAlreadyInRequestedMode
 */
bStatus_t notifyProfile_RegisterAppCBs( notifyProfileCBs_t *appCallbacks )
{
  if ( appCallbacks )
  {
    notifyProfile_AppCBs = appCallbacks;

    return ( SUCCESS );
  }
  else
  {
    return ( bleAlreadyInRequestedMode );
  }
}


/*********************************************************************
 * @fn      notifyProfile_SetParameter
 *
 * @brief   Set a notifyProfile parameter.
 *
 * @param   param - Profile parameter ID
 * @param   len - length of data to right
 * @param   value - pointer to data to write.  This is dependent on
 *          the parameter ID and WILL be cast to the appropriate
 *          data type (example: data type of uint16 will be cast to
 *          uint16 pointer).
 *
 * @return  bStatus_t
 */
bStatus_t notifyProfile_SetParameter( uint8 param, uint8 len, void *value )
{
  bStatus_t ret = SUCCESS;
  switch ( param )
  {
    case ELP1:
      if ( len == ELP1_LEN )
      {
        VOID osal_memcpy( &ELP1Value, value, ELP1_LEN );
        // See if Notification has been enabled 
        GATTServApp_ProcessCharCfg( ELP1Config, &ELP1, FALSE,
        notifyProfileAttrTbl, GATT_NUM_ATTRS( notifyProfileAttrTbl ),
        INVALID_TASK_ID );
      }
      else
      {
        ret = bleInvalidRange;
      }
      break;

    case EPL2:
      if ( len == EPL2_LEN )
      {
        VOID osal_memcpy( &EPL2Value, value, EPL2_LEN );
      }
      else
      {
        ret = bleInvalidRange;
      }
      break;

    default:
      ret = INVALIDPARAMETER;
      break;
  }

  return ( ret );
}

/*********************************************************************
 * @fn      notifyProfile_GetParameter
 *
 * @brief   Get a notifyProfile parameter.
 *
 * @param   param - Profile parameter ID
 * @param   value - pointer to data to put.  This is dependent on
 *          the parameter ID and WILL be cast to the appropriate
 *          data type (example: data type of uint16 will be cast to
 *          uint16 pointer).
 *
 * @return  bStatus_t
 */
bStatus_t notifyProfile_GetParameter( uint8 param, void *value )
{
  bStatus_t ret = SUCCESS;
  switch ( param )
  {
    case ELP1:
      VOID osal_memcpy( value, &ELP1Value, ELP1_LEN );
      break;

    case EPL2:
      VOID osal_memcpy( value, &EPL2Value, EPL2_LEN );
      break;

    default:
      ret = INVALIDPARAMETER;
      break;
  }

  return ( ret );
}

/*********************************************************************
 * @fn          notifyProfile_ReadAttrCB
 *
 * @brief       Read an attribute.
 *
 * @param       connHandle - connection message was received on
 * @param       pAttr - pointer to attribute
 * @param       pValue - pointer to data to be read
 * @param       pLen - length of data to be read
 * @param       offset - offset of the first octet to be read
 * @param       maxLen - maximum length of data to be read
 *
 * @return      Success or Failure
 */
static uint8 notifyProfile_ReadAttrCB( uint16 connHandle, gattAttribute_t *pAttr, 
                            uint8 *pValue, uint8 *pLen, uint16 offset, uint8 maxLen )
{
  bStatus_t status = SUCCESS;

  // If attribute permissions require authorization to read, return error
  if ( gattPermitAuthorRead( pAttr->permissions ) )
  {
    // Insufficient authorization
    return ( ATT_ERR_INSUFFICIENT_AUTHOR );
  }

  // Make sure it's not a blob operation (no attributes in the profile are long)
  if ( offset > 0 )
  {
    return ( ATT_ERR_ATTR_NOT_LONG );
  }

  if ( pAttr->type.len == ATT_BT_UUID_SIZE )
  {
    // 16-bit UUID
    uint16 uuid = BUILD_UINT16( pAttr->type.uuid[0], pAttr->type.uuid[1]);
    switch ( uuid )
    {
      // No need for "GATT_SERVICE_UUID" or "GATT_CLIENT_CHAR_CFG_UUID" cases;
      // gattserverapp handles those reads

      case ELP1_UUID:
        *pLen = ELP1_LEN;
        VOID osal_memcpy( pValue, pAttr->pValue, ELP1_LEN );
        break;

      case EPL2_UUID:
        *pLen = EPL2_LEN;
        VOID osal_memcpy( pValue, pAttr->pValue, EPL2_LEN );
        break;

      default:
        // Should never get here!
        *pLen = 0;
        status = ATT_ERR_ATTR_NOT_FOUND;
        break;
    }
  }
  else
  {
    // 128-bit UUID
    *pLen = 0;
    status = ATT_ERR_INVALID_HANDLE;
  }

  return ( status );
}

/*********************************************************************
 * @fn      notifyProfile_WriteAttrCB
 *
 * @brief   Validate attribute data prior to a write operation
 *
 * @param   connHandle - connection message was received on
 * @param   pAttr - pointer to attribute
 * @param   pValue - pointer to data to be written
 * @param   len - length of data
 * @param   offset - offset of the first octet to be written
 * @param   complete - whether this is the last packet
 * @param   oper - whether to validate and/or write attribute value
 *
 * @return  Success or Failure
 */
static bStatus_t notifyProfile_WriteAttrCB( uint16 connHandle, gattAttribute_t *pAttr,
                                 uint8 *pValue, uint8 len, uint16 offset )
{
  bStatus_t status = SUCCESS;
  uint8 notifyApp = 0xFF;

  // If attribute permissions require authorization to write, return error
  if ( gattPermitAuthorWrite( pAttr->permissions ) )
  {
    // Insufficient authorization
    return ( ATT_ERR_INSUFFICIENT_AUTHOR );
  }

  if ( pAttr->type.len == ATT_BT_UUID_SIZE )
  {
    // 16-bit UUID
    uint16 uuid = BUILD_UINT16( pAttr->type.uuid[0], pAttr->type.uuid[1]);
    switch ( uuid )
    {
      case ELP1_UUID:
        //Validate the value
        // Make sure it's not a blob oper
        if ( offset == 0 )
        {
          if ( len != ELP1_LEN )
          {
            status = ATT_ERR_INVALID_VALUE_SIZE;
          }
        }
        else
        {
          status = ATT_ERR_ATTR_NOT_LONG;
        }

        //Write the value
        if ( status == SUCCESS )
        {
          VOID osal_memcpy( pAttr->pValue, pValue, ELP1_LEN );
          notifyApp = ELP1;
        }
        break;

      case EPL2_UUID:
        //Validate the value
        // Make sure it's not a blob oper
        if ( offset == 0 )
        {
          if ( len != EPL2_LEN )
          {
            status = ATT_ERR_INVALID_VALUE_SIZE;
          }
        }
        else
        {
          status = ATT_ERR_ATTR_NOT_LONG;
        }

        //Write the value
        if ( status == SUCCESS )
        {
          VOID osal_memcpy( pAttr->pValue, pValue, EPL2_LEN );
          notifyApp = EPL2;
        }
        break;


      case GATT_CLIENT_CHAR_CFG_UUID:
        status = GATTServApp_ProcessCCCWriteReq( connHandle, pAttr, pValue, len,
                                                 offset, GATT_CLIENT_CFG_NOTIFY );
        break;

      default:
        status = ATT_ERR_ATTR_NOT_FOUND;
        break;
    }
  }
  else
  {
    // 128-bit UUID
    status = ATT_ERR_INVALID_HANDLE;
  }

  // If a charactersitic value changed then callback function to notify application of change
  if ( (notifyApp != 0xFF ) && notifyProfile_AppCBs && notifyProfile_AppCBs->pfnnotifyProfileChange )
  {
    notifyProfile_AppCBs->pfnnotifyProfileChange( notifyApp );  
  }

  return ( status );
}

/*********************************************************************
 * @fn          notifyProfile_HandleConnStatusCB
 *
 * @brief       notifyProfile link status change handler function.
 *
 * @param       connHandle - connection handle
 * @param       changeType - type of change
 *
 * @return      none
 */
static void notifyProfile_HandleConnStatusCB( uint16 connHandle, uint8 changeType )
{
  // Make sure this is not loopback connection
  if ( connHandle != LOOPBACK_CONNHANDLE )
  {
    // Reset Client Char Config if connection has dropped
    if ( ( changeType == LINKDB_STATUS_UPDATE_REMOVED )      ||
         ( ( changeType == LINKDB_STATUS_UPDATE_STATEFLAGS ) && 
           ( !linkDB_Up( connHandle ) ) ) )
    {
      GATTServApp_InitCharCfg( connHandle, ELP1Config );
    }
  }
}



/*********************************************************************
*********************************************************************/
