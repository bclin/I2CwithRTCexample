/**************************************************************************************************
	Filename:       notifyProfile.h
	Revised:        $Date:2016-07-21 16:45:53 $

	Description:    This file contains the GATT profile definitions and prototypes..

	Copyright 2013 EPLAB National Tsing Hua University. All rights reserved.
	The information contained herein is confidential property of NTHU. 	The material may be used for a personal and non-commercial use only in connection with 	a legitimate academic research purpose. Any attempt to copy, modify, and distribute any portion of this source code or derivative thereof for commercial, political, or propaganda purposes is strictly prohibited. All other uses require explicit written permission from the authors and copyright holders. This copyright statement must be retained in its entirety and displayed in the copyright statement of derived source code or systems.
**************************************************************************************************/

#ifndef NOTIFYPROFILE_H
#define NOTIFYPROFILE_H

#ifdef __cplusplus
extern "C"
{
#endif

/*********************************************************************
 * INCLUDES
 */

/*********************************************************************
 * CONSTANTS
 */

// Profile Parameters
#define ELP1		0
#define EPL2		1

// notifyProfile Profile Service UUID
#define NOTIFYPROFILE_SERV_UUID		0xCCC0

// notifyProfile UUID
#define ELP1_UUID		0xCCC1
#define EPL2_UUID		0xCCC2

// notifyProfile Profile Services bit fields
#define NOTIFYPROFILE_SERVICE		0x00000001

// Length of Characteristics
#define ELP1_LEN		1
#define EPL2_LEN		1

/*********************************************************************
 * TYPEDEFS
 */

/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * Profile Callbacks
 */

// Callback when a characteristic value has changed
typedef NULL_OK void (*notifyProfileChange_t)( uint8 paramID );

typedef struct
{
  notifyProfileChange_t		pfnnotifyProfileChange; // Called when characteristic value changes
} notifyProfileCBs_t;



/*********************************************************************
 * API FUNCTIONS 
 */

/*
 * notifyProfile_AddService- Initializes the GATT Profile service by registering
 *          GATT attributes with the GATT server.
 *
 * @param   services - services to add. This is a bit map and can
 *                     contain more than one service.
 */

extern bStatus_t notifyProfile_AddService( uint32 services );

/*
 * notifyProfile_RegisterAppCBs - Registers the application callback function.
 *                    Only call this function once.
 *
 *    appCallbacks - pointer to application callbacks.
 */
extern bStatus_t notifyProfile_RegisterAppCBs( notifyProfileCBs_t *appCallbacks );

/*
 * notifyProfile_SetParameter - Set a GATT Profile parameter.
 *
 *    param - Profile parameter ID
 *    len - length of data to right
 *    value - pointer to data to write.  This is dependent on
 *          the parameter ID and WILL be cast to the appropriate
 *          data type (example: data type of uint16 will be cast to
 *          uint16 pointer).
 */
extern bStatus_t notifyProfile_SetParameter( uint8 param, uint8 len, void *value );

/*
 * notifyProfile_GetParameter - Get a GATT Profile parameter.
 *
 *    param - Profile parameter ID
 *    value - pointer to data to write.  This is dependent on
 *          the parameter ID and WILL be cast to the appropriate 
 *          data type (example: data type of uint16 will be cast to 
 *          uint16 pointer).
 */
extern bStatus_t notifyProfile_GetParameter( uint8 param, void *value );


/*********************************************************************
*********************************************************************/

#ifdef __cplusplus
}
#endif

#endif /* NOTIFYPROFILE_H */	
