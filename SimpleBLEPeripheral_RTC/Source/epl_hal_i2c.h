 /**	
 *  @ingroup HAL
 *	@defgroup I2C
 *	@{
 *	@file 	epl_hal_i2c.h
 *	@brief 	create software I2C to write or read
 *	
 *	@author	zeroping.tw@gmail.com
 *	@date	2012.11.19
 *  @copyright Copyright 2013 EPLAB National Tsing Hua University. All rights reserved.\n
 *  The information contained herein is confidential property of NTHU. 
 *      The material may be used for a personal and non-commercial use only in connection with 
 *      a legitimate academic research purpose.  
 *      Any attempt to copy, modify, and distribute any portion of this source code or derivative thereof for commercial, 
 *      political, or propaganda purposes is strictly prohibited.  
 *      All other uses require explicit written permission from the authors and copyright holders. 
 *      This copyright statement must be retained in its entirety and displayed 
 *      in the copyright statement of derived source code or systems.
 */

#ifndef EPL_HAL_I2C_H
#define EPL_HAL_I2C_H

#ifdef __cplusplus
extern "C"
{
#endif
  
/***************************************************************************
***************************************************************************/
#include <ioCC2540.h>
#include <hal_types.h>
#include <hal_defs.h>

/** @cond */
#define W2SCL                     P0_6
#define W2SDA                     P0_7
  
#define HIGH                      1  
#define LOW                       0 
/** @endcond */

/*	@brief	Wait for some time to get proper I2C timing
 *	
 *	@param 	[in] none	
 *	@return	none
 */ 
void I2CWait(void);

/*	@brief	Initial I2C	
 *	
 *	@param	[in] none	
 *	@return	none
 */
void I2CInit(void);

/*	@brief	Generates a START(S) condition on the bus.
 *	
 *	@param	[in] none	
 *	@return	none
 */
void I2CStart(void);

/*	@brief	Generates a STOP(P) condition on the bus.
 *	
 *	@param	[in] none	
 *	@return	none
 */
void I2CStop(void);

/*	@brief	Send a Byte to the slave
 *
 *	@param [in] uint8 byte 8 bit date to the slave
 *	@return ack receive from the slave
 */
unsigned char I2CSentByte(uint8 bytedata);

/*	@brief	Receive a Byte from the slave
 *
 *	@param [in] ack  0 or 1 to do ack
 *	@return data - read from the slave
 */
unsigned char I2CReceiveByte( uint8 ack);
/***************************************************************************
***************************************************************************/
  
#ifdef __cplusplus
}
#endif

#endif


/**
* @}
*/